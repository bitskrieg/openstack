#!/bin/bash

#OpenSUSE Bootstrap Script
#This script will install curl (if needed), pull the latest bootstrap script for saltstack
#directly from saltstack, fix the initrd path in grub.conf so the VM will boot properly
#and remove the duplicate entry from fstab

zypper -n install curl
curl -L -o /tmp/bootstrap.sh https://bootstrap.saltstack.com 
chmod +x /tmp/bootstrap.sh
#For additional command line options see:
#https://docs.saltstack.com/en/latest/topics/tutorials/salt_bootstrap.html
/tmp/bootstrap.sh stable
rm /tmp/bootstrap.sh
grub2-mkconfig -o /boot/grub2/grub.cfg
sed -i '1d' /etc/fstab
reboot
